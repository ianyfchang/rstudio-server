FROM bioconductor/release_core2

RUN apt-get update && apt-get -y install poppler-utils openjdk-8-jdk libnetcdf-dev libcairo2-dev less vim 

ADD scripts/install.R /tmp/

# invalidates cache every 24 hours
#ADD http://master.bioconductor.org/todays-date /tmp/

RUN R -f /tmp/install.R

EXPOSE 8787