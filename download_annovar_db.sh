refGene           # FASTA sequences for all annotated transcripts in RefSeq Gene	20170601
dbnsfp35c         # whole-exome SIFT, PolyPhen2 HDIV, PolyPhen2 HVAR, LRT, MutationTaster, MutationAssessor, FATHMM, PROVEAN, MetaSVM, MetaLR, VEST, M-CAP, CADD, GERP++, DANN, fathmm-MKL, Eigen, GenoCanyon, fitCons, PhyloP and SiPhy scores from dbNSFP version 3.5c
intervar_20180118 #InterVar: clinical interpretation of missense variants (indels not supported)	
cosmic70          # COSMIC database version 70 on WGS data
exac03            #ExAC 65000 exome allele frequency data for ALL, AFR (African), AMR (Admixed American), EAS (East Asian), FIN (Finnish), NFE (Non-finnish European), OTH (other), SAS (South Asian)). version 0.3. Left normalization done.
exac03nontcga     #ExAC on non-TCGA samples (updated header)	
exac03nonpsych    #ExAC on non-Psychiatric disease samples (updated header)	
gnomad_exome      #gnomAD exome collection (v2.0.1)
gnomad_genome     #gnomAD genome collection (v2.0.1)	
kaviar_20150923   #170 million Known VARiants from 13K genomes and 64K exomes in 34 projects	
hrcr1             #40 million variants from 32K samples in haplotype reference consortium	
abraom            #2.3 million Brazilian genomic variants
1000g2015aug      # (6 data sets) The 1000G team fixed a bug in chrX frequency calculation. Based on 201508 collection v5b (based on 201305 alignment)
gme               #Great Middle East allele frequency including NWA (northwest Africa), NEA (northeast Africa), AP (Arabian peninsula), Israel, SD (Syrian desert), TP (Turkish peninsula) and CA (Central Asia)
mcap              #M-CAP scores for non-synonymous variants
revel             #REVEL scores for non-synonymous variants
avsnp150          #dbSNP150 with allelic splitting and left-normalization
nci60             #NCI-60 human tumor cell line panel exome sequencing allele frequency data
clinvar_20180603  #Clinvar version 20180603 with separate columns (CLNALLELEID CLNDN CLNDISDB CLNREVSTAT CLNSIG) #CLINVAR database with Variant Clinical Significance (unknown, untested, non-pathogenic, probable-non-pathogenic, probable-pathogenic, pathogenic, drug-response, histocompatibility, other) and Variant disease name
regsnpintron      #prioritize the disease-causing probability of intronic SNVs

g=hg19 #g=hg38

for f in cg69 gwava regsnpintron mitimpact24 icgc21 ensGene refGene dbnsfp35c intervar_20180118 cosmic70 exac03 exac03nontcga exac03nonpsych gnomad_exome gnomad_genome kaviar_20150923 hrcr1 abraom 1000g2015aug gme mcap revel avsnp150 nci60 clinvar_20180603 regsnpintron popfreq_all_20150413 cg46 gnomad211_exome gnomad211_genome
do
/home/ian/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/annotate_variation.pl -buildver $g -downdb -webfrom annovar $f /home/ian/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/
done

# ucsc simple repeat, rmsk
for f in simpleRepeat rmsk targetScanS tfbsConsSites oreganno encRegTfbsClustered  cpgIslandExt refSeqFuncElems microsat
do
/home/ian/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/annotate_variation.pl -buildver $g -downdb $f /home/ian/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/
done


download chromosome fa from ensembl

./gtfToGenePred -genePredExt /lustre_work/stripe_folder/bdp/bdp-data/5db4fe1611675561d3174366/5dbd7dd511675561d317db76/Canis_familiaris.CanFam3.1.gtf canFam3_ensGene.txt
sort -k2,2 -k4,4n canFam3_ensGene.txt > tmp
mv tmp canFam3_ensGene.txt
vim canFam3_ensGene.txt # add 1 to first column


../annovar.2018Apr16/retrieve_seq_from_fasta.pl /home/ian/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/canFam3_ensGene.txt -seqdir /home/ian/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/canFam3_seq -format ensGene -outfile /home/ian/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/canFam3_ensGeneMrna.fa