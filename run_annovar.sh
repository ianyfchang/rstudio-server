cd 09_HAPLOTYPECALLER
for f in *N.HC.VF.g.vcf.gz
do
b=`basename $f .gz`
zcat $f > $b
done

cd 10_MUTECT2
for f in *.somatic.m2.vcf.gz
do
b=`basename $f .gz`
zcat $f > $bgit 
done

for f in ../09_HAPLOTYPECALLER/*N.HC.VF.g.vcf; 
do 
b=`basename $f .vcf`;
~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/table_annovar.pl $f ~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/ -buildver hg38 -out $b -protocol refGene -operation g -nastring . -vcfinput --thread 20 --maxgenethread 20 -polish; 
done


for f in ../10_MUTECT2/*.vcf; 
do 
b=`basename $f .vcf`;
~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/table_annovar.pl $f ~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/ -buildver hg38 -out $b -protocol refGene -operation g -nastring . -vcfinput --thread 20 --maxgenethread 20 -polish; 
done

for f in ../11_VARSCAN2/*.snp.Somatic.vcf; 
do 
b=`basename $f .vcf`;
~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/table_annovar.pl $f ~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/ -buildver hg38 -out ${b}.varscan2 -protocol refGene -operation g -nastring . -vcfinput --thread 20 --maxgenethread 20 -polish; 
done

for f in ../11_VARSCAN2/*.snp.Germline.vcf; 
do 
b=`basename $f .vcf`;
~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/table_annovar.pl $f ~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/ -buildver hg38 -out ${b}.varscan2 -protocol refGene -operation g -nastring . -vcfinput --thread 20 --maxgenethread 20 -polish; 
done

for f in ../12_MUSE/*.vcf; 
do 
b=`basename $f .vcf`;
~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/table_annovar.pl $f ~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/ -buildver hg38 -out ${b} -protocol refGene -operation g -nastring . -vcfinput --thread 20 --maxgenethread 20 -polish; 
done

for f in ../13_SOMATIC_SNIPER/*.snp.filtered.vcf.fp_pass.hc.vcf; 
do 
b=`basename $f .vcf`;
~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/table_annovar.pl $f ~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/ -buildver hg38 -out ${b}.somaticsniper -protocol refGene -operation g -nastring . -vcfinput --thread 20 --maxgenethread 20 -polish; 
done


# SJ
for f in 462N  462P  464N  464P  R459N  R459P  R460N  R460P  R461N  R461P  R462N  R462P  R464N  R464P  R465N  R465P  R467N  R467P  R469N  R469P  R470N  R470P; 
do 
awk -v OFS="\t" '{print $1,$2,$3,0,"-"}' ../2nd_pass/${f}/SJ.out.tab > ${f}.SJ.avinput; b=${f}.SJ;  ~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/annovar.2018Apr16/table_annovar.pl ${f}.SJ.avinput ~/lustre/REFERENCES_FOR_WORKFLOWPLAYER/ANNOVARDB/ -buildver hg38 -out $b -protocol refGene -operation g -nastring .  --thread 30 --maxgenethread 30;  
done
-----------------------------------------------------------------


# HG19

refGene,cpgIslandExt,encRegTfbsClustered,microsat,oreganno,rmsk,simpleRepeat,targetScanS,tfbsConsSites,clinvar_20180603,clinvar_20190821,cosmic70,cosmic89,nci60,abraom,AFR.sites.2015_08,ALL.sites.2015_08,AMR.sites.2015_08,EAS.sites.2015_08,EUR.sites.2015_08,SAS.sites.2015_08,avsnp150,cg46,cg69,dbnsfp35c,exac03,exac03nonpsych,exac03nontcga,gme,gnomad_exome,gnomad_genome,gnomad211_exome,gnomad211_genome,hrcr1,icgc21,intervar_20180118,kaviar_20150923,mcap,mitimpact24,popfreq_all_20150413,regsnpintron,revel

g,r,r,r,r,r,r,r,r,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f


# HG38

encRegTfbsClustered,microsat,oreganno,rmsk,simpleRepeat,gencodeV29,refGene,abraom,AFR.sites.2015_08,ALL.sites.2015_08,AMR.sites.2015_08,EAS.sites.2015_08,EUR.sites.2015_08,SAS.sites.2015_08,avsnp150,clinvar_20180603,clinvar_20190821,cosmic70,cosmic89,cpgIslandExt,dbnsfp35c,exac03nonpsych,exac03nontcga,exac03,gme,gnomad211_exome,gnomad211_genome,gnomad_exome,gnomad_genome,hrcr1,intervar_20180118,kaviar_20150923,nci60,regsnpintron,revel
r,r,r,r,r,g,g,f,f,f,f,f,f,f,f,f,f,f,f,r,f,f,f,f,f,f,f,f,f,f,f,f,f,f,f

"Chr","Start","End","Ref","Alt","encRegTfbsClustered","microsat","oreganno","rmsk","simpleRepeat","Func.gencodeV29","Gene.gencodeV29","GeneDetail.gencodeV29","ExonicFunc.gencodeV29","AAChange.gencodeV29","Func.refGene","Gene.refGene","GeneDetail.refGene","ExonicFunc.refGene","AAChange.refGene","abraom_freq","abraom_filter","abraom_cegh_filter","AFR.sites.2015_08","ALL.sites.2015_08","AMR.sites.2015_08","EAS.sites.2015_08","EUR.sites.2015_08","SAS.sites.2015_08","avsnp150","CLNALLELEID","CLNDN","CLNDISDB","CLNREVSTAT","CLNSIG","clinvar_20190821","cosmic70","cosmic89","cpgIslandExt","SIFT_score","SIFT_converted_rankscore","SIFT_pred","LRT_score","LRT_converted_rankscore","LRT_pred","MutationTaster_score","MutationTaster_converted_rankscore","MutationTaster_pred","MutationAssessor_score","MutationAssessor_score_rankscore","MutationAssessor_pred","FATHMM_score","FATHMM_converted_rankscore","FATHMM_pred","PROVEAN_score","PROVEAN_converted_rankscore","PROVEAN_pred","MetaSVM_score","MetaSVM_rankscore","MetaSVM_pred","MetaLR_score","MetaLR_rankscore","MetaLR_pred","M-CAP_score","M-CAP_rankscore","M-CAP_pred","MutPred_score","MutPred_rankscore","fathmm-MKL_coding_score","fathmm-MKL_coding_rankscore","fathmm-MKL_coding_pred","Eigen_coding_or_noncoding","Eigen-raw","Eigen-PC-raw","GenoCanyon_score","GenoCanyon_score_rankscore","integrated_fitCons_score","integrated_fitCons_score_rankscore","integrated_confidence_value","GERP++_RS","GERP++_RS_rankscore","phyloP100way_vertebrate","phyloP100way_vertebrate_rankscore","phyloP20way_mammalian","phyloP20way_mammalian_rankscore","phastCons100way_vertebrate","phastCons100way_vertebrate_rankscore","phastCons20way_mammalian","phastCons20way_mammalian_rankscore","SiPhy_29way_logOdds","SiPhy_29way_logOdds_rankscore","Interpro_domain","GTEx_V6p_gene","GTEx_V6p_tissue","ExAC_nonpsych_ALL","ExAC_nonpsych_AFR","ExAC_nonpsych_AMR","ExAC_nonpsych_EAS","ExAC_nonpsych_FIN","ExAC_nonpsych_NFE","ExAC_nonpsych_OTH","ExAC_nonpsych_SAS","ExAC_nontcga_ALL","ExAC_nontcga_AFR","ExAC_nontcga_AMR","ExAC_nontcga_EAS","ExAC_nontcga_FIN","ExAC_nontcga_NFE","ExAC_nontcga_OTH","ExAC_nontcga_SAS","ExAC_ALL","ExAC_AFR","ExAC_AMR","ExAC_EAS","ExAC_FIN","ExAC_NFE","ExAC_OTH","ExAC_SAS","GME_AF","GME_NWA","GME_NEA","GME_AP","GME_Israel","GME_SD","GME_TP","GME_CA","gnomad211_exome_AF","gnomad211_exome_AF_popmax","gnomad211_exome_AF_male","gnomad211_exome_AF_female","gnomad211_exome_AF_raw","gnomad211_exome_AF_afr","gnomad211_exome_AF_sas","gnomad211_exome_AF_amr","gnomad211_exome_AF_eas","gnomad211_exome_AF_nfe","gnomad211_exome_AF_fin","gnomad211_exome_AF_asj","gnomad211_exome_AF_oth","gnomad211_exome_non_topmed_AF_popmax","gnomad211_exome_non_neuro_AF_popmax","gnomad211_exome_non_cancer_AF_popmax","gnomad211_exome_controls_AF_popmax","gnomad211_genome_AF","gnomad211_genome_AF_popmax","gnomad211_genome_AF_male","gnomad211_genome_AF_female","gnomad211_genome_AF_raw","gnomad211_genome_AF_afr","gnomad211_genome_AF_sas","gnomad211_genome_AF_amr","gnomad211_genome_AF_eas","gnomad211_genome_AF_nfe","gnomad211_genome_AF_fin","gnomad211_genome_AF_asj","gnomad211_genome_AF_oth","gnomad211_genome_non_topmed_AF_popmax","gnomad211_genome_non_neuro_AF_popmax","gnomad211_genome_non_cancer_AF_popmax","gnomad211_genome_controls_AF_popmax","gnomAD_exome_ALL","gnomAD_exome_AFR","gnomAD_exome_AMR","gnomAD_exome_ASJ","gnomAD_exome_EAS","gnomAD_exome_FIN","gnomAD_exome_NFE","gnomAD_exome_OTH","gnomAD_exome_SAS","gnomAD_genome_ALL","gnomAD_genome_AFR","gnomAD_genome_AMR","gnomAD_genome_ASJ","gnomAD_genome_EAS","gnomAD_genome_FIN","gnomAD_genome_NFE","gnomAD_genome_OTH","HRC_AF","HRC_AC","HRC_AN","HRC_non1000G_AF","HRC_non1000G_AC","HRC_non1000G_AN","InterVar_automated","PVS1","PS1","PS2","PS3","PS4","PM1","PM2","PM3","PM4","PM5","PM6","PP1","PP2","PP3","PP4","PP5","BA1","BS1","BS2","BS3","BS4","BP1","BP2","BP3","BP4","BP5","BP6","BP7","Kaviar_AF","Kaviar_AC","Kaviar_AN","nci60","regsnp_fpr","regsnp_disease","regsnp_splicing_site","REVEL","Otherinfo_0","Otherinfo_1","Otherinfo_DP","CHROM","POS","ID","REF","ALT","QUAL","FILTER","INFO","FORMAT","SAMPLE"